# High-level Technical Description
Decentralized Liquid Democracy consists of two components: the liquid democracy blockchain and the legislation repository.

## Liquid Democracy Blockchain
There are three major functions of the liquid democracy blockchain:
* Voter registration
* Legislation Proposition
* Voting

### Entities
To support the three functions listed above, the liquid democracy blockchain recognizes five entities:
* Registrar: A registrar's sole responsibility is to manage (add, update, remove) voters and tabulators in the blockchain. They do not have the ability to vote
* Super Registrar: A super-user registrar that manages registrars. They also do not have the ability to vote
* Voter: A voter registered to the blockchain is allowed to vote on any issue provided the voter registration is active
* Delegate: Any voter that is currently being delegated votes
* Tabulator: The sole function of a tabulator is to count the results of a vote at closing time and write it to the chain. Tabulators do not vote.

Every entity in the chain is registered to the chain with a public key (the person associated with the entity retains the private key and keeps it secret). The public key can be updated by a registrar at the appropriate level.

### Voter Registration
Voters are registered to the chain by a registrar. The voter's registration includes their public key, a unique identifier, and a mandatory expiry date. The expiry date is mandatory by default to force registration renewal in order to account for voters who become ineligible over time.

The unique identifier is associated with the voter’s personal information in an external datastore accessible only to the registrars. This ensures that no personal voter data is written to the blockchain where it would be viewable by anyone.

#### Trust
Keeping the voter’s information out of the blockchain means that we can’t verify each voter in the chain, so we are required to trust in the integrity of our registrars. This is net-neutral with our current system of democracy however, and we also gain the ability to monitor what registrars are doing and count the number of current active voters easily which can then be cross-referenced with other information like census totals to see if there are any potential issues. Regardless, regular auditing of the registrars and the external voter database is highly recommended.

### Proposing legislation
Legislation can be proposed for consideration by a valid voter provided that it meets these criteria:

* It is integrable with the target branch via a trivial merge
* It is integrable with any already proposed, pending legislation via a trivial merge or has a voting window beginning entirely after such legislation

These rules put the onus for ensuring integrability on the proposer and also prevent someone from introducing legislation that would block the integrability of legislation that is currently under consideration. 

Legislation is proposed with a vote open time and a vote closing time that are consistent with the configuration of the chain. It also requires a URL that points to the location of a repository where the branch can be found.

### Voting
Once the voting period opens for a piece of legislation, all voters that were eligible to vote at the opening time may vote by writing their vote to the chain. Their vote counts for themselves and any voters that are delegating their vote to the voter but that haven't voted on the proposal themselves.

#### Delegation
A voter may optionally delegate their vote to another voter in general or on a per-issue basis. General delegation must have an expiry time to ensure that voters don't completely disengage from the democratic process.

Delegation can be revoked at any time and can always be overridden by the voter if they wish to vote directly on an issue themselves.

Delegation is also chaining, so if a delegate delegates their vote to another voter their delegated votes are carried with them. Chained delegation must not form a cycle (i.e. a delegator must not delegate their vote to a voter that is currently delegating their vote to the delegator).

#### Vote closing
When the closing time for a given issue is reached, votes are counted and a tabulator writes the result of the vote to the chain. If the result passes, the proposal is merged to the target branch in the legislation repository. If its integration invalidates any pending proposal (i.e. makes its merge non-trivial), the proposal is cancelled and a new proposal must be made taking into account the new changes.

To further validate the tally, a checksum of the valid votes is persisted with the result on the chain.

## Legislation Repository
The legislation repository is a Git repository that serves as the living record of all active laws.

### Branching
The main branch represents the current state of the repository. It is recommended that legislation under consideration lives in a separate branch until it is ready to be voted on for integration with main.

### Format
The legislation in the repository should use only basic formatting (i.e. markdown), or none at all to keep the record free of noise due to formatting changes.
