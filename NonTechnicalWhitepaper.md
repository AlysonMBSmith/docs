# Decentralized Liquid Democracy Non-technical Whitepaper
Democracies across the world have experienced a resurgence in extreme and authoritarian politics driven by a sense of disenfranchisement among the electorate as people increasingly feel as though their governments aren't working for them. There is no singular root cause for this, but traditional representative democracy suffers from structural issues that each contribute to this problem.

## Corruption
In a representative democracy, power is wielded by a very small portion of the electorate making it highly susceptible to corruption which leads to the prioritization of moneyed interests over those of the less affluent populace.

## Accountability
The primary method for holding politicians accountable in a representative democracy is to vote them out of office. Elections are held infrequently however, and in many cases feature candidates that are all largely undesirable. As a result, politicians tend to not be held to account even for particularly egregious behaviour.

## Agility

> The greatest danger that now faces liberal democracy is that the revolution in information technology will make dictatorships more efficient than democracies.

- Yuval Noah Harari

Democracy works through achieving consensus between many individuals which can take time while dictatorships have no such requirement. This makes democracy inherently slower to react to new developments than autocracy which can exacerbate issues that need a quick intervention by government.

# Decentralized Liquid Democracy
Voting is integral to the fabric of democratic society but despite a multitude of advancements in almost every other facet of our life, the democratic process remains largely untouched by the information age. This is largely because there has been no solution to-date that addresses the issues with traditional representative democracy while maintaining certain guarantees that the existing system currently provides. The goal of decentralized liquid democracy is to fix the structural issues of our current system while maintaining the guarantees it provides.

## Technologies
Decentralized liquid democracy aims to solve these issues using two decentralized technologies: Blockchain and Git.

### Blockchain
Blockchain technology offers the following benefits:

* Decentralized: There is no single source of record. Anyone can download a copy of the chain and synchronize it with other copies as needed
* Immutable: Once data has been written to the chain it can't be changed without rewriting everything that was written after it
* Verifiable: Anyone that has the chain can verify the legitimacy of every transaction within it

For these reasons it will be used as the technology backing the voter registry and liquid democracy datastores.

### Git
Git is a decentralized version control system - it will be used to provide the source of record for legislation.

## Solution Description
Decentralized Liquid Democracy consists of two components: the liquid democracy blockchain and the legislation repository.

### Liquid Democracy Blockchain
The liquid democracy blockchain consists of voters, voter registrars, tabulators, and legislation proposals and their results. It is a direct democracy system that allows for vote delegation on a general or per-issue basis.

### Legislation Repository
The legislation repository is a Git repository that serves as the living record of all active laws. New legislation that passes a vote in the liquid democracy blockchain is automatically integrated to the legislation repository.

The repository specifies that it is for legislation since that is the goal for Decentralized Liquid Democracy but it can contain anything that can be represented in text.

### Technical Details
See [the high-level technical description for more information](technical/HighLevelDescription.md).

## Benefits
DLD solves the structural issues in traditional representative democracy and offers some additional benefits too.

### Fluid Representation
In a traditional representative democracy, large groups of voters are mapped to a single representative using a variety of methods (e.g. first past the post, proportional representation). This is done because voting on every proposal is unreasonable to expect of most people which is a major problem for simple direct democracy, but the representative strategies deployed frequently erase voters' representation (e.g. if a voter votes for a losing candidate under first-past-the-post, their vote no longer has a voice until the next election).

DLD solves both the representation problem since voters are always in control of their vote, and direct democracy's engagement problem because voters are in control of how much or how little they involve themselves in the process.

### Accountability
DLD is highly accountable since a voter can change or revoke their delegation at any time.

### Corruption
DLD eliminates elections and therefore individual campaigns and campaign contributions which are a frequently-used vector for corruption.

The power that an individual delegate wields is more volatile and generally, but not necessarily, lower than a representative in traditional democracy which makes the delegate harder to corrupt.

### Agile
There are no legislative sessions in DLD by default and since everything is synchronized over the network, voters can propose and debate legislation at any time. As such, DLD can respond to new information substantially more quickly than traditional representative democracy.

### Secret
Voters are associated with a public key and an additional unique ID that corresponds with the personal information of the voter in an external system. This enables a voter to start anonymous to everyone but the voter registrars provided that the voter does not reveal their associated public key or unique ID. If they do become known they can return to anonymity through their voter ID getting expired on the registration chain and a new unique ID added.

### Validatable
Any two individuals with a copy of the datastores should be able to validate everything that has been written and arrive at the same answer.

### Decentralized
There is no central source of record - any two people whose datastores agree on the past should be able to synchronize with incoming data.

### Secure
Only voters with a valid public key can write to the chain.
